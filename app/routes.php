<?php
/**
 * Created by PhpStorm.
 * User: georgealmeida
 * Date: 9/11/17
 * Time: 19:44
 */

/**
 * @var \ticket\core\Router $router
 */

//routes EVENTO
$router->get('eventos/:id/usuario', 'EventoController@eventosUsuario');
$router->get('eventos/nuevo', 'EventoController@formNuevo','gestor');
$router->post('eventos/nuevo', 'EventoController@crear', 'gestor');
$router->get('eventos/:id','EventoController@mostrar');
$router->delete('eventos/:id','EventoController@eliminar', 'gestor');
$router->get('eventos/:id/edit','EventoController@edit','gestor');
$router->post('eventos/:id/update','EventoController@update', 'gestor');
$router->get('eventos','EventoController@listar'); //mostrar todos los eventos? o solo los de el usuario que los ha creado
$router->get('','EventoController@listar');
$router->post('','EventoController@listar');
$router->post('eventos','EventoController@listar');

$router->get('pagos/:id','FacturaController@pago');
$router->post('pagar/:id','FacturaController@pagar');
$router->get('factura/:id','FacturaController@mostrar');

$router->get('captcha','CaptchaController@mostrar');

//routes USUARIO
$router->get('usuario/:id','UsuarioController@mostrar');
$router->get('usuarios', 'UsuarioController@lista', 'admin');
$router->post('usuarios', 'UsuarioController@lista', 'admin');
$router->delete('usuarios/:id','UsuarioController@eliminar', 'admin');
$router->post('usuarios/:id/modify','UsuarioController@modify', 'admin');

$router->get('usuarios/:id/update', 'UsuarioController@formUpdate');
$router->post('usuarios/:id/update', 'UsuarioController@update');
$router->post('usuarios/:id/updateImage', 'UsuarioController@updateImage');
//routes AUTH
$router->get('login','AuthController@login');
$router->post('check-login','AuthController@checkLogin');
$router->get('logout', 'AuthController@logout', 'comprador');


$router->post('registroGestor', 'UsuarioController@usuarioGestor');
$router->post('registroComprador', 'UsuarioController@usuarioComprador');

$router->get('registro', 'UsuarioController@formNuevo');

//IDIOMAS
$router->get('idiomas/es', 'IdiomaController@idiomaEsp');

$router->get('idiomas/en', 'IdiomaController@idiomaEng');
/*$router->get('login', 'AuthController@login');
$router->post('check-login','AuthController@checkLogin');*/
