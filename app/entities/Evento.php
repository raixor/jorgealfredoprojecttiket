<?php
/**
 * Created by PhpStorm.
 * User: georgealmeida
 * Date: 10/11/17
 * Time: 17:43
 */

namespace ticket\app\entities;

use ticket\app\helpers\SubirFichero;
use ticket\app\helpers\Reescalado;

class Evento
{
    use SubirFichero;
    use Reescalado;

    private $id;
    private $nombre;
    private $imagen;
    private $fecha;
    private $precio;
    private $localidad;
    private $descripcion;
    private $direccion;
    private $categoria;
    private $entradasDisp;
    private $entradasTota;
    private $creador;
    private $hora;
    private $fechaIni;
    private $fechaFin;

    /**
     * @return mixed
     */
    public function getFechaIni()
    {
        return $this->fechaIni;
    }

    /**
     * @param mixed $fechaIni
     */
    public function setFechaIni($fechaIni)
    {
        $this->fechaIni = $fechaIni;
    }

    /**
     * @return mixed
     */
    public function getFechaFin()
    {
        return $this->fechaFin;
    }

    /**
     * @param mixed $fechaFin
     */
    public function setFechaFin($fechaFin)
    {
        $this->fechaFin = $fechaFin;
    }

    /**
     * @return mixed
     */
    public function getHora()
    {
        return $this->hora;
    }

    /**
     * @param mixed $hora
     */
    public function setHora($hora)
    {
        $this->hora = $hora;
    }

    /**
     * Evento constructor.
     * @param $id
     * @param $nombre
     * @param $imagen
     * @param $fecha
     * @param $precio
     * @param $localidad
     * @param $descripcion
     * @param $direccion
     * @param $categoria
     * @param $entradasDisp
     * @param $entradasTota
     * @param $creador
     */


    public function rellenarEvento()
    {

        $this->setNombre($_POST['nombre']);
        $this->setPrecio($_POST['precio']);
        $this->setCategoria($_POST['categoria']);
        $this->setLocalidad($_POST['localidad']);
        $this->setDescripcion($_POST['descripcion']);
        $this->setDireccion($_POST['direccion']);
        $this->setEntradasTota($_POST['entradasTota']);
        $this->setFecha($_POST['fecha']);
        $this->setFechaIni($_POST['fechaIni']);
        $this->setFechaFin($_POST['fechaFin']);
        $this->setHora($_POST['hora']);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * @param mixed $imagen
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;
    }

    /**
     * @return mixed
     */
    public function getFecha()
    {
        return $this->fecha;
    }
    public function getFechaMostrar()
    {
        return date_format(date_create($this->fecha), 'd/m/Y H:i');
    }

    /**
     * @param mixed $fecha
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * @return mixed
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * @param mixed $precio
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;
    }

    /**
     * @return mixed
     */
    public function getLocalidad()
    {
        return $this->localidad;
    }

    /**
     * @param mixed $localidad
     */
    public function setLocalidad($localidad)
    {
        $this->localidad = $localidad;
    }

    /**
     * @return mixed
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @param mixed $descripcion
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }

    /**
     * @return mixed
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * @param mixed $direccion
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;
    }

    /**
     * @return mixed
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * @param mixed $categoria
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;
    }

    /**
     * @return mixed
     */
    public function getEntradasDisp()
    {
        return $this->entradasDisp;
    }

    /**
     * @param mixed $entradasDisp
     */
    public function setEntradasDisp($entradasDisp)
    {
        $this->entradasDisp = $entradasDisp;
    }

    /**
     * @return mixed
     */
    public function getEntradasTota()
    {
        return $this->entradasTota;
    }

    /**
     * @param mixed $entradasTota
     */
    public function setEntradasTota($entradasTota)
    {
        $this->entradasTota = $entradasTota;
    }

    /**
     * @return mixed
     */
    public function getCreador()
    {
        return $this->creador;
    }

    /**
     * @param mixed $creador
     */
    public function setCreador($creador)
    {
        $this->creador = $creador;
    }

    public function __toString()
    {
        return "$this->nombre $this->precio $this->localidad";
    }
}