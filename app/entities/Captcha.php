<?php
/**
 * Created by PhpStorm.
 * User: georgealmeida
 * Date: 1/01/18
 * Time: 14:36
 */

namespace ticket\app\entities;


class Captcha
{
    public static function mostrar()
    {
        $longitud = rand(5,8);
        $caracteres = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $captcha = '';

        for($i = 0; $i < $longitud; $i++){
            $captcha .= $caracteres[rand(0, strlen($caracteres)-1)];
        }

        $_SESSION['captcha'] = $captcha;

        $ancho = 500;
        $alto = 180;
        $fuente_tamano = rand(30,40);

        $img = imagecreatetruecolor($ancho,$alto);

        $blanco  = ImageColorAllocate($img,255,255,255);

        $x = $fuente_tamano;
        $y = ($alto / 2) + 25;

        imagettftext($img, $fuente_tamano,0,$x,$y,$blanco,'',$captcha);

        header("Content-Type: image/jpeg");
        ImageJpeg($img);
        ImageDestroy($img);
    }

    public static function validar($valor)
    {
        if(!isset($_SESSION['captcha'])) return false;
        return $_SESSION['captcha'] == $valor;
    }
}