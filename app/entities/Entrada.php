<?php
/**
 * Created by PhpStorm.
 * User: georgealmeida
 * Date: 10/11/17
 * Time: 18:05
 */

namespace ticket\entities;


class Entrada
{
    private $id;
    private $codigo;
    private $evento;
    private $factura;
    private $precioVenta;
    private $precio;


    /**
     * Entradas constructor.
     * @param $id
     * @param $codigo
     * @param $evento
     * @param $factura
     * @param $precioVenta
     * @param $precio
     */
    public function rellenarFactura()
    {
        $this->setPrecio($_POST['precio']);
    }

    public function __construct($id, $codigo, $evento, $factura, $precioVenta, $precio)
    {
        $this->id = $id;
        $this->codigo = $codigo;
        $this->evento = $evento;
        $this->factura = $factura;
        $this->precioVenta = $precioVenta;
        $this->precio = $precio;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * @param mixed $codigo
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;
    }

    /**
     * @return mixed
     */
    public function getEvento()
    {
        return $this->evento;
    }

    /**
     * @param mixed $evento
     */
    public function setEvento($evento)
    {
        $this->evento = $evento;
    }

    /**
     * @return mixed
     */
    public function getFactura()
    {
        return $this->factura;
    }

    /**
     * @param mixed $factura
     */
    public function setFactura($factura)
    {
        $this->factura = $factura;
    }

    /**
     * @return mixed
     */
    public function getPrecioVenta()
    {
        return $this->precioVenta;
    }

    /**
     * @param mixed $precioVenta
     */
    public function setPrecioVenta($precioVenta)
    {
        $this->precioVenta = $precioVenta;
    }

    /**
     * @return mixed
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * @param mixed $precio
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;
    }

    public function __toString()
    {
        return "$this->evento $this->codigo $this->precio";
    }
}