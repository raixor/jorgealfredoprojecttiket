<?php
/**
 * Created by PhpStorm.
 * User: georgealmeida
 * Date: 12/11/17
 * Time: 19:53
 */

namespace ticket\app\controllers;

use ticket\app\entities\Usuario;
use ticket\core\App;
use Exception;
use ticket\app\exceptions\UploadException;
use ticket\core\Response;
use ticket\core\Security;

class UsuarioController
{
    public function formNuevo()
    {
        Response::renderView('UsuarioNuevo',[]);
    }

    public function formUpdate($id)
    {
        $usuario = App::get('database')->find('Usuarios','Usuario',$id);
        Response::renderView('UsuarioUpdate',['usuario' => $usuario]);

    }

    private function gestionaImagenUsuario(Usuario $usuario)
    {
        try
        {
            $usuario->setDirUpload('./uploads/usuarios');
            $usuario->setNombreCampoFile('avatar');
            $usuario->setTiposPermitidos(
                [
                    'image/jpg',
                    'image/jpeg',
                    'image/gif',
                    'image/png'
                ]);
            $usuario->setAvatar($usuario->subeImagen());
            $usuario->Rees($usuario->getAvatar(),'/usuarios/',100,100);
        }
        catch (UploadException $uploadException)
        {
            if ($uploadException->getFileError() === UPLOAD_ERR_NO_FILE)
                $usuario->setAvatar('anonimo.png');
            else
            {
                throw $uploadException;
            }
        }
    }

    public function usuarioComprador()
    {
        $usuarioNuevo = new Usuario();
        $usuarioNuevo->setRol('comprador');
        $this->usuarioNuevo($usuarioNuevo);
        //TODO autolog de usuario comprador?
    }
    public function usuarioGestor()
    {
        $usuarioNuevo = new Usuario();
        $usuarioNuevo->setRol('admin');
        $this->usuarioNuevo($usuarioNuevo);

        $usuarioLgoin = App::get('database')->findOneBy(
            'Usuarios', 'Usuario',
            [
                'email' => $_POST['email'],
            ]);

        var_dump($usuarioLgoin);

        if (!is_null($usuarioLgoin) && Security::checkPassword($_POST['password'], $usuarioLgoin->getSalt(), $usuarioLgoin->getPassword()))
        {
            $_SESSION['usuario'] = $usuarioLgoin->getId();
            App::get('router')->redirect('eventos');
        }
        else{
            $_SESSION['error'] = 'El usuario y password introducidos no existen';

            App::get('router')->redirect('eventos');
        }
    }
    private function usuarioNuevo(Usuario $usuarioNuevo )
    {
        try
        {
            //validaciones
            $usuarioEmail = App::get('database')->findOneBy(
                'Usuarios', 'Usuario',
                [
                    'email' => $_POST['email'],
                ]);

            if($_POST['password'] != $_POST['rPassword']){
                $_SESSION['error'] = 'Los password no coinciden';
                App::get('router')->redirect('eventos');
            }
            elseif(!is_null($usuarioEmail)){
                $_SESSION['error'] = 'El email introducido ya tiene cuenta';
                App::get('router')->redirect('eventos');
            }
            else{
                $usuarioNuevo->rellearUsuario();
                $salt = Security::getSalt();
                $password = Security::encrypt($usuarioNuevo->getPassword(),$salt) ;
                $usuarioNuevo->setSalt($salt);

                $usuarioNuevo->getAvatar() ? $this->gestionaImagenUsuario($usuarioNuevo) : $usuarioNuevo->setAvatar('/uploads/usuarios/anonimo.png');

                $parameters = [
                    'nombre' => $usuarioNuevo->getNombre(),
                    'avatar' => $usuarioNuevo->getAvatar(),
                    'email' => $usuarioNuevo->getEmail(),
                    'password' => $password,
                    'rol' => $usuarioNuevo->getRol(),
                    'salt' => $usuarioNuevo->getSalt(),
                ];
                var_dump($parameters);
                App::get('database')->insert('Usuarios',$parameters);
            }
        }
        catch(Exception $exception)
        {
            $error = $exception->getMessage();
        }
    }

    public function mostrar($id)
    {
        $yo = false;
        $tieneEventos = false ;
        if( $id === App::get('user')->getId()){
            $usuario = App::get('user');
            $yo = true;
        }else{
            $usuario = App::get('database')->find('Usuarios','Usuario',$id);
            $tieneEventos = count(App::get('database')->findBy('Eventos','Evento',['creador' => $id],$withLike = true)) > 0 ? true : false;
        }
        Response:: renderView (
            'Usuario',
            [
                'usuarioDetail'=>$usuario,
                'yo'=>$yo,
                'tieneEventos' => $tieneEventos
            ]
        );
    }

    public function lista()
    {
        if ( isset($_POST['rol']) && !empty($_POST['rol'])){
            $usuarios = App::get('database')->findBy('Usuarios','Usuario',[
                'rol'=> $_POST['rol']
            ], $withLike = true);
        }
        else{
            $usuarios = App::get('database')->findAll('Usuarios','Usuario');
        }

        Response:: renderView (
            'Usuarios',
            [
                'usuarios'=>$usuarios
            ]
        );
    }

    public function eliminar($id)
    {

        try{
            if(count(App::get('database')->findBy('Eventos','Evento',['creador' => $id],$withLike = true)) > 0){
                $resultado[] = [
                    'code' => '200',
                    'message' => 'El evento no ha sido eliminado correctamente, el usuario tiene eventos'
                ];
            }else{
                $filters =[
                    'id'=> $id
                ];
                App::get('database')->delete('Usuarios', $filters);
                $resultado[] = [
                    'code' => '200',
                    'message' => 'El evento ha sido eliminado correctamente, el usuario tiene eventos'
                ];
            }

            echo json_encode($resultado);
        }catch (Exception $exception){

        }
    }

    public function modify($id)
    {
        if(isset($_POST['rol']) && !empty($_POST['rol'])){
            $parameters = [
                'rol' => $_POST['rol'],
            ];

            $filters = [
                'id' => $id
            ];

            App::get('database')->update('Usuarios', $parameters, $filters);

            App::get('router')->redirect('usuarios');
        }
    }

    public function update($id)
    {
        if(isset($_POST['nPassword']) && isset($_POST['nRPassword']) &&
            !empty(trim($_POST['nPassword'])) && !empty(trim($_POST['nRPassword'])) &&
        $_POST['nPassword'] == $_POST['rNPassword']){

            $usuario = App::get('database')->find('Usuarios','Usuario',$id);
            $salt = Security::getSalt();

            $newPassword = Security::encrypt($_POST['nPassword'],$salt) ;
            $usuario->setSalt($salt);

            $parameters = [
                'salt' => $usuario->getSalt(),
                'password' => $newPassword,
            ];

            $filters = [
                'id' => $id
            ];

            App::get('database')->update('Usuarios', $parameters, $filters);

            App::get('router')->redirect('usuario/'.$id);
        }
        $_SESSION['error'] = 'Error en los passwords';
        App::get('router')->redirect('usuarios/'.$id.'/update');
    }

    public function updateImage($id)
    {
            $usuario = App::get('database')->find('Usuarios','Usuario',$id);
            $this->gestionaImagenUsuario($usuario);

            $parameters = [
                'avatar' => $usuario->getAvatar(),
            ];

            $filters = [
                'id' => $id
            ];

            App::get('database')->update('Usuarios', $parameters, $filters);

            App::get('router')->redirect('usuario/'.$id);
    }
}