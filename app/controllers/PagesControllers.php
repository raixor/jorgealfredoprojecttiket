<?php
/**
 * Created by PhpStorm.
 * User: georgealmeida
 * Date: 15/11/17
 * Time: 17:39
 */

namespace ticket\app\controllers;


use ticket\core\Response;

class PagesControllers
{
    public function notFound()
    {
        header('HTTP/1.1 404 Not Found', true, 404);
        Response::renderView('404');
    }

}