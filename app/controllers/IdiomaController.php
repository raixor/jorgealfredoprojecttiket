<?php
/**
 * Created by PhpStorm.
 * User: georgealmeida
 * Date: 4/12/17
 * Time: 17:37
 */

namespace ticket\app\controllers;


use ticket\core\App;

class IdiomaController
{
    public function idiomaEsp()
    {
        $_SESSION['language'] = 'es_ES.utf8';

        App::get('router')->redirect('');
    }

    public function idiomaEng()
    {
        $_SESSION['language'] = 'en_GB.utf8';

        App::get('router')->redirect('');
    }
}