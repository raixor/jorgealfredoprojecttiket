<?php
/**
 * Created by PhpStorm.
 * User: georgealmeida
 * Date: 12/11/17
 * Time: 19:51
 */

namespace ticket\app\controllers;

use ticket\core\App;
use ticket\core\Response;
use ticket\app\entities\Factura;


class FacturaController
{
    public function pago($id)
    {
        $eventoPay = App::get('database')->find('Eventos','Evento',$id);

        Response::renderView('pagar',[
            'evento' => $eventoPay
        ]);
    }

    public function pagar($id)
    {
        if (isset($_POST['entradas'])) {


            $nuevaFactura = new Factura();
            $nuevaFactura->setEntradas($_POST['entradas']);
            $usuario = App::get('user');

            $eventoPay = App::get('database')->find('Eventos', 'Evento', $id);

            $parameters = [
                'evento' => $eventoPay->getId(),
                'comprador' => $usuario->getId(),
                'entradas' => $nuevaFactura->getEntradas(),
                'total' => $nuevaFactura->getEntradas() * $eventoPay->getPrecio(),
            ];
            $total = $nuevaFactura->getEntradas() * $eventoPay->getPrecio();
            $entradas = $nuevaFactura->getEntradas();
           /* App::get('database')->insert('Facturas', $parameters);*/

            Response::renderView('Factura', [
                'evento' => $eventoPay,
                'total' => $total,
                'entradas' => $entradas
            ]);
        }
        else{
            Response::renderView('404');
        }

    }
    public function mostrar($id)
    {
        $eventoPay =  $_SESSION['eventoPay'];
        Response::renderView('Factura',['evento'=>$eventoPay]);
    }
}