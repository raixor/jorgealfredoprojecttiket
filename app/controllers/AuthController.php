<?php
/**
 * Created by PhpStorm.
 * User: georgealmeida
 * Date: 20/11/17
 * Time: 16:43
 */

namespace ticket\app\controllers;

use ticket\core\App;
use ticket\core\Response;
use ticket\core\Security;

class AuthController
{
    public function login()
    {
        $error = null;
        if (isset($_SESSION['error']))
        {
            $error = $_SESSION['error'];
            $_SESSION['error'] = null;
        }
        Response::renderView('login', ['error' => $error]);
    }

    public function unauthorized()
    {
        header('HTTP/1.1 403 Forbidden', true, 403);
        Response::renderView('403');
    }

    public function checkLogin()
    {
        if (isset($_POST['email']) && !empty($_POST['email']) &&
            isset($_POST['password']) && !empty($_POST['password']))
        {
            $usuario = App::get('database')->findOneBy(
                'Usuarios', 'Usuario',
                [
                    'email' => $_POST['email'],
                ]);
            echo "hola";

            var_dump($usuario);

            if (!is_null($usuario) && Security::checkPassword($_POST['password'], $usuario->getSalt(), $usuario->getPassword()))
            {
                $_SESSION['usuario'] = $usuario->getId();
                App::get('router')->redirect('eventos');
            }
            else{
                $_SESSION['error'] = 'El usuario y password introducidos no existen';

                App::get('router')->redirect('');
            }
        }
        else
        {
            $_SESSION['error'] = 'Debes introducir el usuario y el password';

            App::get('router')->redirect('');
        }
    }

    public function logout()
    {
        if (isset ($_SESSION['usuario']))
        {
            $_SESSION['usuario'] = null;
            unset ($_SESSION['usuario']);
        }

        App::get('router')->redirect('');
    }

}