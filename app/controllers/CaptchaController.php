<?php
/**
 * Created by PhpStorm.
 * User: georgealmeida
 * Date: 1/01/18
 * Time: 15:09
 */

namespace ticket\app\controllers;


use ticket\core\Response;

class CaptchaController
{
    public function mostrar()
    {
        Response::renderView('captcha',[]);
    }
}