<?php
/**
 * Created by PhpStorm.
 * User: georgealmeida
 * Date: 12/11/17
 * Time: 16:44
 */

namespace ticket\app\controllers;

use ticket\app\entities\Evento;
use ticket\core\App;
use Exception;
use ticket\app\exceptions\UploadException;
use ticket\core\Response;


class EventoController
{
    public function edit($id)
    {
        $evento = App::get('database')->find('Eventos','Evento',$id);

        Response::renderView('form-evento',[
            'evento'=>$evento
        ]);
    }
    public function formNuevo()
    {
        Response:: renderView (
            'form-evento',
            []
        );
    }

    public function mostrar($id)
    {
        $usuario = App::get('user');

        $evento = App::get('database')->find('Eventos','Evento',$id);
        $creador = App::get('database')->find('Usuarios','Usuario',$evento->getCreador());
        $_SESSION['eventoPay'] = $evento;
        Response:: renderView (
            'Evento',
            [
                'evento'=>$evento,
                'creador'=>$creador,
                'usuario'=>$usuario
            ]
        );
    }

    public function eventosUsuario($id)
    {
        $eventos = App::get('database')->findBy('Eventos','Evento',
            [
                'creador'=>$id,
            ], $withLike = true);

        Response:: renderView (
            'EventosView',
            [
                'eventos'=>$eventos,
            ]
        );
    }
    public function listar()
    {

        /*$eventos = App::get('database')->findAll('Eventos','Evento');*/
        $busqueda='';
        $localidad='';
        $categoria='';
        $usuario = App::get('user');
        //TODO buscar creador para cada evento $creador = App::get('database')->find('Usuario','Usuario',$eventos->);
        if (isset($_POST['busqueda']) && !empty($_POST['busqueda']))
        {
            $busqueda = $_POST['busqueda'];

            $eventos = App::get('database')->findBy('Eventos','Evento',
                [
                    'nombre'=>$busqueda,
                    'descripcion'=>$busqueda
                ], $withLike = true, $withDate = true);
        }
        /*isset($_POST['categoria']) && !empty($_POST['categoria']) || isset($_POST['localidad']) && !empty($_POST['localidad']) || */
        else if (isset($_POST['categoria']) && !empty($_POST['categoria']) || isset($_POST['localidad']) && !empty($_POST['localidad']) || isset($_POST['fecha']) && !empty($_POST['fecha'])){
            $fechaAhora = date('Y-m-j');
            $fecha= isset($_POST['fecha']) && !empty($_POST['fecha']) ? $_POST['fecha'] : $fechaAhora;
            $localidad= isset($_POST['localidad']) && !empty($_POST['localidad']) ? $_POST['localidad'] : '';
            $categoria= isset($_POST['categoria']) && !empty($_POST['categoria']) ? $_POST['categoria'] : '';

            if(!isset($_POST['fecha']) || empty($_POST['fecha'])){
                $eventos = App::get('database')->findBy('Eventos','Evento',
                    [
                        'localidad'=>$localidad,
                        'categoria'=>$categoria,
                    ], $withLike = true, $withDate = true);
            }
            else if($fecha == 'manana'){
                $fechaSelect= date ('Y-m-d', strtotime ( "$fechaAhora +1 day"));
                $eventos = App::get('database')->findBy('Eventos','Evento',
                    [
                        'localidad'=>$localidad,
                        'categoria'=>$categoria,
                        'fecha'=>$fechaSelect
                    ], $withLike = true, $withDate = true);
            }elseif ($fecha == 'estaSemana'){
                $eventos = App::get('database')->findBy('Eventos','Evento',
                    [
                        'localidad'=>$localidad,
                        'categoria'=>$categoria,
                    ], $withLike = true, $withDate = true);

                $eventos = array_filter($eventos,function($evento){
                    $fechaAhora = date('Y-m-j');
                    $fechaSelect= date ('Y-m-d', strtotime ( "$fechaAhora +7 day"));

                    if( strtotime($evento->getFecha()) >= strtotime($fechaAhora) && strtotime($evento->getFecha()) < strtotime($fechaSelect)){
                        return $evento;
                    }
                });
            }elseif ($fecha == 'esteMes'){
                $eventos = App::get('database')->findBy('Eventos','Evento',
                    [
                        'localidad'=>$localidad,
                        'categoria'=>$categoria,
                    ], $withLike = true, $withDate = true);

                $eventos = array_filter($eventos,function($evento){
                    $fechaAhora = date('Y-m-j H:i:s');
                    $fechaSelect= date ('Y-m-d H:i:s', strtotime ( "$fechaAhora +30 day"));

                    if( strtotime($evento->getFecha()) >= strtotime($fechaAhora) && strtotime($evento->getFecha()) < strtotime($fechaSelect)){
                        return $evento;
                    }
                });
            }elseif ($fecha == 'esteFinde'){
                $eventos = App::get('database')->findBy('Eventos','Evento',
                    [
                        'localidad'=>$localidad,
                        'categoria'=>$categoria,
                    ], $withLike = true, $withDate = true);

                $eventos = array_filter($eventos,function($evento){
                    $diaSemana=date("w");
                    if($diaSemana==0)
                        $diaSemana=7;
                    $ultimoDia = date('Y-m-d',mktime(0, 0, 0, date("m")  , date("d")+(7-$diaSemana), date("Y")));
                    $diaSabado = date('Y-m-d',strtotime ( "$ultimoDia -1 day"));

                    if( strtotime($evento->getFecha()) >= strtotime($diaSabado) && strtotime($evento->getFecha()) < strtotime($ultimoDia)){
                        return $evento;
                    }
                });
            }
        }
        else
        {
            $eventos = App::get('database')->findAllDateAfterNow('Eventos','Evento');
        }
        Response::renderView('EventosView',[
            'eventos'=>$eventos,
            'busqueda'=>$busqueda,
            'localidad'=>$localidad,
            'categoria'=>$categoria,
            'usuario'=>$usuario
        ]);

    }

    private function gestionaImagenEvento(Evento $evento)
    {

        try
        {
            $evento->setDirUpload('./uploads/eventos');
            $evento->setNombreCampoFile('imagen');
            $evento->setTiposPermitidos(
                [
                    'image/jpg',
                    'image/jpeg',
                    'image/gif',
                    'image/png'
                ]);
            $evento->setImagen($evento->subeImagen());
            //echo "eeeee".explode('/',$evento->getImagen())[3];
            $evento->Rees($evento->getImagen(),'/eventos/',300,180);
        }
        catch (UploadException $uploadException)
        {
            if ($uploadException->getFileError() === UPLOAD_ERR_NO_FILE)
                $evento->setImagen(__DIR__.'event.png');
            else
            {
                throw $uploadException;
            }
        }
    }

    private function comprobaciones()
    {
        if (!isset($_POST['nombre']) || empty(trim($_POST['nombre'])) || !isset($_POST['localidad']) || empty(trim($_POST['localidad'])) ||
            !isset($_POST['precio']) || empty(trim($_POST['precio'])) || !isset($_POST['direccion']) || empty(trim($_POST['direccion'])) ||
            !isset($_POST['entradasTota']) || empty(trim($_POST['entradasTota'])) || !isset($_POST['fecha']) || empty($_POST['fecha']) ||
            !isset($_POST['fechaIni']) || empty($_POST['fechaIni']) || !isset($_POST['fechaFin']) || empty($_POST['fechaFin']) ||
            !isset($_POST['hora']) || empty($_POST['hora']) || !isset($_POST['descripcion']) || empty($_POST['descripcion']) )
        {

            return true;
        }
        return false;
    }
    public function crear()
    {
        try
        {
            $nuevoEvento = new Evento();
            if ($this->comprobaciones()){
                $_SESSION['error'] = 'No puedes dejar campos vacios ';
                Response:: renderView (
                    'form-evento',
                    []
                );
                return;
            } ;
            $nuevoEvento->rellenarEvento();
            $usuario = App::get('user');

            $this->gestionaImagenEvento($nuevoEvento);
            $parameters = [
                'nombre' => $nuevoEvento->getNombre(),
                'descripcion' => $nuevoEvento->getDescripcion(),
                'categoria' => $nuevoEvento->getCategoria(),
                'entradasTota' => $nuevoEvento->getEntradasTota(),
                'entradasDisp' => $nuevoEvento->getEntradasTota(),
                'imagen' => $nuevoEvento->getImagen(),
                'precio' => $nuevoEvento->getPrecio(),
                'fecha' => $nuevoEvento->getFecha()." ".$nuevoEvento->getHora(),
                'fechaIni' => $nuevoEvento->getFechaIni(),
                'fechaFin' => $nuevoEvento->getFechaFin(),
                'direccion' => $nuevoEvento->getDireccion(),
                'localidad' => $nuevoEvento->getLocalidad(),
                'categoria' => $nuevoEvento->getCategoria(),
                'creador' => $usuario->getId(),
            ];

            App::get('database')->insert('Eventos',$parameters);
            App::get('router')->redirect('');
            var_dump($parameters);
        }
        catch(Exception $exception)
        {
            $error = $exception->getMessage();
        }
    }

    public function update($id)
    {
        $updateEvento = App::get('database')->find('Eventos', 'Evento', $id);
        if ($this->comprobaciones())
        {
            $_SESSION['error'] = 'No puedes dejar campos vacios ';
            Response:: renderView (
                'form-evento',
                [
                    'evento'=>$updateEvento,
                ]
            );
            return;
        }

        try
        {
            //TODO falla la imagen al actualizar miralo
            $updateEvento ->rellenarEvento();
            $this->gestionaImagenEvento($updateEvento);
            $parameters = [
                'nombre' => $updateEvento ->getNombre(),
                'descripcion' => $updateEvento ->getDescripcion(),
                'categoria' => $updateEvento ->getCategoria(),
                'entradasTota' => $updateEvento ->getEntradasTota(),
                'precio' => $updateEvento ->getPrecio(),
                'fecha' => $updateEvento ->getFecha()." ".$updateEvento->getHora(),
                'direccion' => $updateEvento ->getDireccion(),
                'localidad' => $updateEvento ->getLocalidad(),
                'fechaIni' => $updateEvento->getFechaIni(),
                'fechaFin' => $updateEvento->getFechaFin(),
                'imagen' => $updateEvento->getImagen(),
                'categoria' => $updateEvento ->getCategoria()
            ];

            $filters = [
                'id' => $updateEvento ->getId()
            ];
            App::get('database')->update('Eventos', $parameters, $filters);

            App::get('router')->redirect('eventos');
        }
        catch(Exception $exception)
        {
            throw $exception;
        }
    }

    public function eliminar($id)
    {
        try{
            $filters =[
                'id'=> $id,
                'creador' => App::get('user')->getId(),
            ];
            App::get('database')->delete('Eventos', $filters);

            $resultado[] = [
                'code' => '200',
                'message' => 'El evento ha sido eliminado correctamente'
            ];
            echo json_encode($resultado);
        }catch (Exception $exception){

        }
    }
}