<?php
/**
 * Created by PhpStorm.
 * User: georgealmeida
 * Date: 9/11/17
 * Time: 19:16
 */

return array(
    'database' => array(
        'name' => 'ticket',
        'username' => 'ticketUsu',
        'password' => '1234',
        'connection' => 'mysql:host=localhost',
        'options' => array(
            PDO:: MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
            PDO:: ATTR_ERRMODE => PDO:: ERRMODE_EXCEPTION ,
            PDO:: ATTR_PERSISTENT => true
        )
    ),
    'logs' => array(
        'name' => 'Registro Error',
        'file' => '../logs/ticket-error.log'
    ),
    'security' => array(
        'roles' => array(
            'admin'=>3,
            'gestor'=>2,
            'comprador'=>1
        )
    )
);
