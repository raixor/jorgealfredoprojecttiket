<div class="new-event-container">
    <div class="inputs-container new-event">
        <div class="title">
            <?= isset($evento) ? "Editar evento" : "Nuevo Evento"?>
        </div>
        <form id="newEvent" action="<?= isset($evento) ? '/eventos/' . $evento->getId() . '/update' : '/eventos/nuevo' ?>" method="post" enctype="multipart/form-data">
            <div class="group-field">
                <input type="text" name="nombre" id="nombre" maxlength="19" required value="<?= isset($evento) ? $evento->getNombre() : '' ?>">
                <label for="name">Nombre evento</label>
            </div>
            <div class="group-field">
                <input type="text" name="localidad" id="localidad" required value="<?= isset($evento) ? $evento->getLocalidad() : '' ?>">
                <label for="location">Localidad</label>
            </div>
            <div class="group-field">
                <input type="number" name="precio" id="precio" required max="999" value="<?= isset($evento) ? $evento->getPrecio() : '' ?>">
                <label for="price">Price</label>
            </div>
            <div class="group-field">
                <input type="text" name="direccion" id="direccion" required value="<?= isset($evento) ? $evento->getDireccion() : '' ?>">
                <label for="direction">Direction</label>
            </div>

            <div class="group-field">
                <input type="number" name="entradasTota" id="entradasTota" required value="<?= isset($evento) ? $evento->getEntradasTota() : ''?>">
                <label for="tickets">Nº Entradas</label>
            </div>
            <input type="text" name="fecha" id="fecha" placeholder="Fecha dd/mm/aaaa" onfocus="(this.type='date')"
                   required value="<?= isset($evento) ? explode(" ", $evento->getFecha())[0] : ''?>">
            <input type="text" name="fechaIni" id="fechaIni" placeholder="Fecha inicio de ventas dd/mm/aaaa" onfocus="(this.type='date')"
                   required value="<?= isset($evento) ? explode(" ", $evento->getFechaIni())[0] : ''?>">
            <input type="text" name="fechaFin" id="fechaFin" placeholder="Fecha fin de ventas dd/mm/aaaa" onfocus="(this.type='date')"
                   required value="<?= isset($evento) ? explode(" ", $evento->getFecha())[0] : ''?>">
            <input type="time" name="hora" id="hora" value="<?= isset($evento) ? explode(" ", $evento->getFecha())[1] : ''?>">
            <textarea name="descripcion" id="descripcion" cols="30" rows="10" placeholder="Descripcion..."
                      required ><?= isset($evento) ? $evento->getDescripcion() : ''?></textarea>
            <div class="group-field">
                <p >Categoria:
                    <select name="categoria" class="select">
                        <option value="concierto" <?= isset($evento) && $evento->getCategoria() === "concierto" ? "selected" : "";?>>Concierto</option>
                        <option value="musical" <?= isset($evento) && $evento->getCategoria() === "musical" ? "selected" : "";?>>Musical</option>
                        <option value="teatro" <?= isset($evento) && $evento->getCategoria() === "teatro" ? "selected" : "";?>>Teatro</option>
                        <option value="exposiciones" <?= isset($evento) && $evento->getCategoria() === "exposiciones" ? "selected" : "";?>>Exposiciones</option>
                    </select>
                </p>

            </div>

            <div class="group-field">
                <label for="imagen" class="btn-upload-image">Añadir imagen</label>
                <input type="file" name="imagen" id="imagen" multiple>
            </div>
            <button type="submit" class="btn"><?= isset($evento) ? "EDIT": "CREAR"?></button>
        </form>
    </div>
    <div class="card edit" id="card-edit">
        <div class="card-image">
            <img src="http://via.placeholder.com/700x420" alt="event" id="img-card">
        </div>
        <div class="card-content">
                <span class="card-title text-dark">
                    Título de evento
                </span>
            <p>
                Descripcion
            </p>
        </div>
        <div class="card-action">
            <i class="fa fa-share-alt fa-3" aria-hidden="true"></i>
            <i class="fa fa-bookmark-o" aria-hidden="true"></i>
            <div class="price">
                <i class="fa fa-money" aria-hidden="true"></i>
                <p>000</p>
                <p class="euro">€</p>
            </div>
        </div>
    </div>
</div>


