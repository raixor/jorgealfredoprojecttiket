<?php
$alto = 200;
$ancho = 200;
$imagen = imagecreatetruecolor($ancho, $alto);
$blanco = imagecolorallocate($imagen, 255, 255, 255);
$azul = imagecolorallocate($imagen, 0, 0, 64);
// Dibujamos la imagen
imagefill($imagen, 0, 0, $azul);
imageline($imagen, 0, 0, $ancho, $alto, $blanco);
imagestring($imagen, 4, 50, 150, 'DWES', $blanco);
// Generación de la imagen.
header('content-type: image/png');
imagepng ($imagen);
// Liberamos la imagen de memoria.
imagedestroy($imagen);

/**
 * Created by PhpStorm.
 * User: georgealmeida
 * Date: 1/01/18
 * Time: 15:07
 */