<!--Modales de registro y login no sabemos si se quedarán aquí-->

<main class="events-container">

    <?php foreach ($eventos as $evento): ?>
        <article class="card">
            <div class="eventDetail">
                <a href="eventos/<?= $evento->getId(); ?>">
                    <div class="card-image">
                        <img src="<?= '/uploads/eventos/thumbnails/RESIZED_' . $evento->getImagen(); ?>" alt="img-event">
                    </div>
                    <div class="card-content">
                     <span class="card-title activator text-dark">
                        <?= $evento->getNombre(); ?>
                     </span>
                        <p>
                            <?= substr($evento->getDescripcion(), 0, 195) . "..."; ?>
                        </p>
                        <p class="fecha-hora"><?= explode(" ",$evento->getFechaMostrar())[0]; ?></p>
                        <p class="fecha-hora"><?= explode(" ",$evento->getFechaMostrar())[1]; ?></p>
                    </div>
                </a>
            </div>
            <div class="card-action">
                <?php if ( isset($usuario)) :?>
                    <?php if ( isset($usuario) && $evento->getCreador() !== $usuario->getId()) :?>
                        <i class="fa fa-share-alt fa-3" aria-hidden="true"></i>
                        <?php else:?>
                        <a aria-hidden="true" id="enlace-eliminar-<?= $evento->getId() ?>" class="enlace-eliminar fa fa-trash" href="/eventos/<?= $evento->getId() ?>"></a>
                    <?php endif;?>
                <?php else:?>
                    <i class="fa fa-share-alt fa-3" aria-hidden="true"></i>
                <?php endif;?>
                <a href=""></a>
                <div class="price">
                    <i class="fa fa-money" aria-hidden="true"></i>
                    <p><?= $evento->getPrecio(); ?></p>
                    <p class="euro">€</p>
                </div>
                <?php if (isset($usuario) && $evento->getCreador() === $usuario->getId()) :?>
                    <a  href="/eventos/<?=$evento->getId()?>/edit" class="fa fa-pencil" aria-hidden="true"></a>
                <?php endif;?>
            </div>
        </article>
    <?php endforeach?>
    <script src="js/crud.js"></script>
</main>
