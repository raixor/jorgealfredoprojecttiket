<div class="event-pay-container">
    <div class="title-pay">
        <p>Factura de <?= $evento->getNombre()?></p>
    </div>

    <div class="body-pay">
        <div class="pay-content">
            <div class="content">
                <p for="entradas">Entradas:<?=$entradas?></p>
                <p>Precio de entrada: <?= $evento->getPrecio()?>€</p>
            </div>

        </div>
        <div class="ticket">
            <div class="imageTicket">
                <img src="<?= 'img/1-ticket.jpg'?>" alt="event">
            </div>
            <div class="mid">
            </div>
            <div class="contentTicket">
                <span class="card-ticket">
                    <?= $evento->getNombre()?>
                </span>
                <div class="content">
                    <div class="group">
                        <p class="character">
                            Date:
                        </p>
                        <p>
                            <?= explode(" ",$evento->getFechaMostrar())[0]?>
                        </p>
                    </div>

                    <div class="group">
                        <p class="character">
                            Location:
                        </p>
                        <p>
                            <?= $evento->getLocalidad()?>
                        </p>
                    </div>
                    <div class="group">
                        <p class="character">
                            Zone:
                        </p>
                        <p>
                            VIP
                        </p>
                    </div>
                    <div class="group">
                        <p class="character">
                            Time:
                        </p>
                        <p>
                            <?= explode(" ",$evento->getFecha())[1]?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <!--TODO Poner diferentes entradas si son VIP o no
    <div class="ticket">
        <div class="imageTicket">
            <img src="<?/*= 'img/1-ticket.jpg'*/?>" alt="event">
        </div>
        <div class="mid">
        </div>
        <div class="contentTicket">
                <span class="card-ticket">
                    Arenal Sound
                </span>
            <div class="content">
                <div class="group">
                    <p class="character">
                        Date:
                    </p>
                    <p>
                        23/45/2002
                    </p>
                </div>

                <div class="group">
                    <p class="character">
                        Location:
                    </p>
                    <p>
                        Alicante
                    </p>
                </div>
                <div class="group">
                    <p class="character">
                        Zone:
                    </p>
                    <p>
                        VIP
                    </p>
                </div>
                <div class="group">
                    <p class="character">
                        Time:
                    </p>
                    <p>
                        16:00
                    </p>
                </div>
            </div>
        </div>
        <!--
    <div class="circleTicket"></div>
    </div>-->
    </div>
    <div class="bot-pagos">
        <div class="precios">
            <div class="text">
                <p>Total: </p>
            </div>
            <div class="numbers">
                <p> <?= $total ?>€</p>
            </div>
        </div>
    </div>


</div>

<?php
/**
 * Created by PhpStorm.
 * User: georgealmeida
 * Date: 4/01/18
 * Time: 22:24
 */