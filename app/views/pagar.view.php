<div class="event-pay-container">
    <div class="title-pay">
        <p>Entradas para <?= $evento->getNombre()?></p>
    </div>
    <form id="formPagos" action="/pagar/<?= $evento->getId()?>"" method="post" enctype="multipart/form-data">
        <div class="body-pay">
            <div class="pay-content">
                <div class="content">
                    <label for="entradasPay">Entradas: </label>
                    <input type="number" id="entradasPay" name="entradas" value="1" min="1">
                </div>
                <p>Precio: <?= $evento->getPrecio()?>€</p>

            </div>
            <div class="ticket">
                <div class="imageTicket">
                    <img src="/uploads/eventos/<?=$evento->getImagen() ?>" alt="event">
                </div>
                <div class="mid">
                </div>
                <div class="contentTicket">
                    <span class="card-ticket">
                        <?= $evento->getNombre()?>
                    </span>
                    <div class="content">
                        <div class="group">
                            <p class="character">
                                Date:
                            </p>
                            <p>
                                <?= explode(" ",$evento->getFechaMostrar())[0]?>
                            </p>
                        </div>

                        <div class="group">
                            <p class="character">
                                Location:
                            </p>
                            <p>
                                <?= $evento->getLocalidad()?>
                            </p>
                        </div>
                        <div class="group">
                            <p class="character">
                                Time:
                            </p>
                            <p>
                                <?= explode(" ",$evento->getFechaMostrar())[1]?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <!--TODO Poner diferentes entradas si son VIP o no
        <div class="ticket">
            <div class="imageTicket">
                <img src="<?/*= 'img/1-ticket.jpg'*/?>" alt="event">
            </div>
            <div class="mid">
            </div>
            <div class="contentTicket">
                    <span class="card-ticket">
                        Arenal Sound
                    </span>
                <div class="content">
                    <div class="group">
                        <p class="character">
                            Date:
                        </p>
                        <p>
                            23/45/2002
                        </p>
                    </div>

                    <div class="group">
                        <p class="character">
                            Location:
                        </p>
                        <p>
                            Alicante
                        </p>
                    </div>
                    <div class="group">
                        <p class="character">
                            Zone:
                        </p>
                        <p>
                            VIP
                        </p>
                    </div>
                    <div class="group">
                        <p class="character">
                            Time:
                        </p>
                        <p>
                            16:00
                        </p>
                    </div>
                </div>
            </div>
            <!--
        <div class="circleTicket"></div>
        </div>-->
        </div>
        <div class="bot-pagos">
            <div class="precios">
                <div class="text">
                    <p>Total: </p>
                </div>
                <div class="numbers">
                    <p> 23€</p>
                </div>
            </div>
            <button type="submit" class="btn">Pagar</button>
        </div>
    </form>

</div>
<?php
/**
 * Created by PhpStorm.
 * User: georgealmeida
 * Date: 31/12/17
 * Time: 17:41
 */