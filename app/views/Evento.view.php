<!--TODO La imagen de fondo tienes que ponerla con img por con un backgrpung no la vas a poder cambiar en el php-->
<div class="event-detail-container">
    <div class="card edit detail" style="background-image: url('<?= "/uploads/eventos/".$evento->getImagen()?>')">
        <div class="card-content">
            <div class="section-avatar">
                <a href="<?='/eventos/'.$creador->getId().'/usuario'?>">
                    <img src="<?= '/uploads/usuarios/'.$creador->getAvatar(); ?>" alt="Avatar" class="avatar">
                </a>
            </div>
            <div class="card-content-top">
                  <span class="card-title ">
                     <?= $evento->getNombre(); ?>
                  </span>
                <p>
                    <?= $evento->getLocalidad()." ";?><i class="fa fa-map-marker" aria-hidden="true"></i>
                </p>
            </div>

            <p>
                <?= $evento->getDescripcion();?>
            </p>
            <p class="card-content-date">
                Fecha/Hora: <?= " ".$evento->getFecha();?>
            </p>
            <p class="card-content-date-ini">
                Inicio de ventas: <?= " ".explode(" ",$evento->getFechaIni())[0];?>
            </p>
            <p class="card-content-date-fin">
                Fin de ventas: <?= " ".explode(" ",$evento->getFechaFin())[0];?>
            </p>
            <div class="btns-eventDetail messa-buy ">
                <a href="<?= $usuario ? "/mensaje" : "/registro"; ?>" class="btn btn-buy button">Mensaje</a>
                <a href="<?= $usuario ? "/pagos/".$evento->getId() : "/registro"; ?>" class="btn btn-buy button">Comprar</a>
            </div>
        </div>
        <div class="card-action">
            <div class="btns-eventDetail">
                <i class="fa fa-share-alt fa-3" aria-hidden="true"></i>
            </div>
            <div class="price btns-eventDetail">
                <i class="fa fa-money" aria-hidden="true"></i>
                <p><?= $evento->getPrecio();?></p>
                <p class="euro">€</p>
            </div>
        </div>
    </div>
    <div class="card-conditions">
        <h1>
            Condiciones generales y reglas
        </h1>
        <p>
            Entradas Festival Arenal Sound 2018 Fecha: del 31 de julio al 5 de agosto de 2018 Lugar: Recinto Arenal Sound - Burriana
            (Castellón) Menores de edad: Todos los menores de 18 años tienen que rellenar este formulario, imprimir el
            documento y.
            Presentar la autorización firmada por el padre/madre/tutor Si tienes 16 o 17 años, es suficiente
            con presentar la autorización firmada. Si tienes menos de 16 años además del documento firmado debes ir acompañado
            del padre/madre/tutor, hola que tal como estamos.
            Entradas Festival Arenal Sound 2018 Fecha: del 31 de julio al 5 de agosto de 2018 Lugar: Recinto Arenal Sound - Burriana
            (Castellón) Menores de edad: Todos los menores de 18 años tienen que rellenar este formulario, imprimir el
            documento y presentar la autorización firmada por el padre/madre/tutor Si tienes 16 o 17 años, es suficiente
            con presentar la autorización firmada. Si tienes menos de 16 años además del documento firmado debes ir acompañado
            del padre/madre/tutor.
        </p>
    </div>
</div>
<?php
/**
 * Created by PhpStorm.
 * User: georgealmeida
 * Date: 13/11/17
 * Time: 18:32
 */