<!DOCTYPE html>
<html lang="en">
<head>
    <title>Tickevent</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <link href="/css/style.css" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <meta name="theme-color" content="#083756" />
    <script src="/js/index.js"></script>
    <link rel="icon" type="image/png" href="/img/favicon.ico" />
</head>
<body>
<div class="container">
    <?php include __DIR__ . '/menu.part.php' ?>
    <section>
        <div id="registroModal" class="modal">
            <div class="modal-content">
                <div class="inputs-container">
                    <div class="title">
                        <p> Únete a Tickevent!</p>
                        <label for="cerrar-modal" id="closeRegister">X</label>
                    </div>
                    <form class="register" action="/registroGestor" method="post" id="formRegister" enctype="multipart/form-data">
                        <input type="email" name="email" id="emailGestor" placeholder="Email@email.com" autocomplete="off" required>
                        <input type="text" name="nombre" id="nombreGestor" placeholder="Tu nombre" required autocomplete="off">
                        <input type="password" name="password" id="passwordGestor" placeholder="Password" pattern="(\d|\w){4,}" required>
                        <input type="password" name="rPassword" id="rPasswordGestor" placeholder="Repeat password" pattern="(\d|\w){4,}" required>
                        <button type="submit" class="btn">SUBMIT</button>
                    </form>
                </div>
            </div>
        </div>
        <div id="filterModal" class="modal">
            <div class="modal-content">
                <div class="inputs-container">
                    <div class="title">
                        <p> Filtros</p>
                        <label for="cerrar-modal" id="closeFilter">X</label>
                    </div>
                    <form id="form-filtrar" action="<?= $_SERVER['REQUEST_URI'] === '/usuarios' ? '/usuarios' : '/'?>" method="POST" class="register">
                        <?php if ($_SERVER['REQUEST_URI'] !== '/usuarios') :?>
                            <input type="text" name="localidad" placeholder="Localidad..." value="<?= isset($localidad) ? $localidad : '' ?>">
                            <input type="text" name="categoria" placeholder="Categoria..." value="<?= isset($categoria) ? $categoria : '' ?>">
                            <div  class="date">
                                <div class="radio-buttons">
                                    <label for="manana">Mañana</label>
                                    <input type="radio" name="fecha" value="manana" id="manana">
                                </div>
                                <div class="radio-buttons">
                                    <label for="estaSemana">Esta semana</label>
                                    <input type="radio" name="fecha" value="estaSemana" id="estaSemana">
                                </div>
                                <div class="radio-buttons">
                                    <label for="esteFinde">Fin de semana</label>
                                    <input type="radio" name="fecha" value="esteFinde" id="esteFinde">
                                </div>
                                <div class="radio-buttons">
                                    <label for="esteMes">Este mes</label>
                                    <input type="radio" name="fecha" value="esteMes" id="esteMes">
                                </div>
                            </div>
                        <?php else : ?>
                        <select name="rol" class="select">
                            <option value="admin" >Admin</option>
                            <option value="gestor" >Gestor</option>
                            <option value="comprador" >Comprador</option>
                        </select>
                        <?php endif; ?>

                        <button type="submit" class="btn">FILTRAR</button>
                    </form>
                </div>
            </div>
        </div>
        <div id="loginModal" class="modal">
            <div class="modal-content">
                <div class="inputs-container">
                    <div class="title">
                        <p> Login </p>
                        <label for="cerrar-modal" id="closeLogin">X</label>
                    </div>
                    <form class="login" action="/check-login" method="post" enctype="multipart/form-data" id="formLogin">
                        <input type="email" name="email" placeholder="Email@email.com" required autocomplete="off">

                        <input type="password" name="password" id="passwordLogin" placeholder="Password" required>

                        <button type="submit" class="btn">LOGIN</button>

                        <div class="registrarse">
                            ¿No tienes cuenta?
                            <a href="#" id="btn-registro">Regístrate</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <?php if(isset($_SESSION['error']) && $_SESSION['error']):?>
        <div class="error"><?= $_SESSION['error']?></div>
    <?php endif; $_SESSION['error']='';?>

    <!--<header>
        <h1>Ticket</h1>
        <ul>
            <li><a href="/idiomas/en">English</a></li>
            <li><a href="/idiomas/es">Español</a></li>
        </ul>

    </header>-->
