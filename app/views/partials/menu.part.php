<header>
    <div class="back-panel">
        <div class="bar-top">
            <nav class="bar-top-nav">
                <a class="selected inicio" href="/eventos">
                    <img src="/img/ticket.png" alt="event">
                </a>
                <?php if($_SERVER['REQUEST_URI'] !== '/usuarios'): ?>
                    <form id="form-busqueda" action="/" method="POST">
                        <input type="search" name="busqueda" placeholder="Buscar evento..." value="<?= isset($busqueda) ? $busqueda : '' ?>">
                        <input type="submit" name="enviar" value="&#xF002;">
                    </form>
                <?php endif; ?>
                <!--<input type="search" name="busqueda" id="search" placeholder="&#xF002; Search..."/>-->
                <div class="div-filter">
                    <a class="btn btn-filter" id="filters"><?= gettext("Filtros")?> <p>&#xf063;</p></a>
                    <!--<form id="form-filtrar" action="/" method="POST">
                        <input type="text" name="localidad" placeholder="Localidad..." value="<?/*= isset($localidad) ? $localidad : '' */?>">
                        <input type="text" name="categoria" placeholder="Categoria..." value="<?/*= isset($categoria) ? $categoria : '' */?>">
                        <div  class="date">
                            <div class="radio-buttons">
                                <label for="manana">Mañana</label>
                                <input type="radio" name="fecha" value="manana" id="manana">
                            </div>
                            <div class="radio-buttons">
                                <label for="estaSemana">Esta semana</label>
                                <input type="radio" name="fecha" value="estaSemana" id="estaSemana">
                            </div>
                            <div class="radio-buttons">
                                <label for="esteFinde">Fin de semana</label>
                                <input type="radio" name="fecha" value="esteFinde" id="esteFinde">
                            </div>
                            <div class="radio-buttons">
                                <label for="esteMes">Este mes</label>
                                <input type="radio" name="fecha" value="esteMes" id="esteMes">
                            </div>
                        </div>

                        <input type="submit" name="enviar" value="Filtrar">-->
                    </form>
                </div>

                <?php if ($usuario) :?>
                    <div class="menu-big">
                        <?php if ($usuario->getRol() == 'gestor') : ?>
                            <a href="/eventos/nuevo">Crear evento</a>
                        <?php elseif ($usuario->getRol() == 'comprador') :?>
                            <a href="/eventos/nuevo">Mis entradas</a>
                        <?php else :?>
                            <a href="/usuarios">Usuarios</a>
                        <?php endif; ?>
                        <a href="#">Mensajes</a>
                        <a href="/usuario/<?= $usuario->getId() ?>">Perfil-<?= $usuario->getNombre() ?></a>
                    </div>
                <?php else : ?>
                    <?php if ($_SERVER['REQUEST_URI'] != '/registro') :?>
                    <div class="btn-login-register">
                        <div class="bar-btns">
                            <a href="#" class="btn" id="btn-login">Entrar</a>
                        </div>
                        <div class="bar-btns bar-btn-register">
                            <a href="#" class="btn" id="btn-register">Registro</a>
                        </div>
                    </div>
                    <?php endif; ?>
                <?php endif; ?>
                <div class="perfil">
                    <a id="ico-login" href="./profile.html">
                        <i class="fa fa-user-circle" aria-hidden="true"></i>
                    </a>
                </div>
            </nav>
        </div>
    </div>
</header>
<section class="menu-panel mobile">
    <div class="menu-inputs">
        <div class="bar-down">
            <a href="">
                <i class="fa fa-comments " aria-hidden="true"></i>
            </a>
            <a href="">
                <i class="fa fa-ellipsis-h" aria-hidden="true"></i>
            </a>
            <a href="">
                <i class="fa fa-ticket" aria-hidden="true"></i>
            </a>
            <a href="./new-event.html">
                <i class="fa fa-plus" aria-hidden="true"></i>
            </a>
        </div>
    </div>
</section>












