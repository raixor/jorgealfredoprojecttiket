<main class="settings-container">
    <div class="title">Profile</div>
    <div class="section-avatar">
        <img src="<?= '/uploads/usuarios/'.$usuarioDetail->getAvatar(); ?>" alt="Avatar" class="avatar">
    </div>
    <div class="name-modify">
        <p><?= $usuarioDetail->getNombre()?></p>
    </div>

    <div class="bar"><?= gettext('Correo') ?><p class="to-modify"><?= $usuarioDetail->getEmail(); ?></p></div>
    <?php if ($yo):?>
        <a class="change-password" href="/usuarios/<?= $usuarioDetail->getId() ?>/update">
            <div class="bar">Cambiar password</div>
        </a>
        <div class="bar">Language<p class="to-modify">English</p></div>
        <div class="bar"><a class="logout" href="/logout">Logout</a></div>
        <form class="Modificar-avatar" action="/usuarios/<?= $usuario->getId()?>/updateImage" method="post" enctype="multipart/form-data">
            <label for="avatarUpdate" class="btn-upload-image">Modificar avatar</label>
            <input type="file" name="avatar" id="avatarUpdate" multiple>
            <button type="submit" class="btn" >Cambiar</button>
        </form>
    <?php endif;?>
    <div class="bar">Rol: <?= $usuarioDetail->getRol()?></div>
    <?php if (!$yo):?>
    <form id="newEvent" action="/usuarios/<?= $usuarioDetail->getId()?>/modify" method="post" enctype="multipart/form-data">
        <select name="rol" class="select">
            <option value="comprador" <?= $usuarioDetail->getRol() === "comprador" ? "selected" : "";?>>Comprador</option>
            <option value="gestor" <?= $usuarioDetail->getRol() === "gestor" ? "selected" : "";?>>Gestor</option>
        </select>
        <button type="submit" class="btn">Modificar</button>
    </form>
    <?php endif;?>
</main>
