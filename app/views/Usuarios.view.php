<main class="usuarios">
    <?php foreach ($usuarios as $usuario): ?>
        <article class="card">
            <div class="usuarioDetail">
                <a class="usuario-a" href="usuario/<?= $usuario->getId(); ?>">
                    <div class="card-image">
                        <img src="<?= '/uploads/usuarios/' . $usuario->getAvatar(); ?>" alt="img-event">
                    </div>
                    <div class="card-content">
                         <span class="card-title activator text-dark">
                            <?= $usuario->getNombre(); ?>
                         </span>
                        <p><?= $usuario->getRol(); ?></p>
                    </div>
                    <div class="btns-usuarios">
                        <a  id="enlace-eliminar-<?= $usuario->getId() ?>" class="enlace-eliminar btn eliminar" href="/usuarios/<?= $usuario->getId() ?>">Eliminar</a>
                        <a  id="enlace-modificar-<?= $usuario->getId() ?>" class="modificar btn " href="usuario/<?= $usuario->getId(); ?>">Modificar</a>
                    </div>
                </a>
            </div>
        </article>
    <?php endforeach?>
        <script src="js/crud.js"></script>
</main>
<?php
/**
 * Created by PhpStorm.
 * User: georgealmeida
 * Date: 6/01/18
 * Time: 18:49
 */