<?php
/**
 * Created by PhpStorm.
 * User: georgealmeida
 * Date: 15/11/17
 * Time: 17:42
 */

namespace ticket\core;


class Response
{
    public static function renderView($name, $data = array())
    {
        extract ($data);

        $usuario = App::get('user');

        ob_start ();

        require __DIR__ . "/../app/views/$name.view.php";

        $mainContent = ob_get_clean ();

        require __DIR__ . '/../app/views/layout.view.php';
    }
}