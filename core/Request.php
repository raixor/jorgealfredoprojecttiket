<?php
/**
 * Created by PhpStorm.
 * User: georgealmeida
 * Date: 9/11/17
 * Time: 19:14
 */

namespace ticket\core;

class Request
{
    public static function uri()
    {
        return trim (
            parse_url(
                $_SERVER['REQUEST_URI'],
                PHP_URL_PATH
            ),
            '/');
    }

    public static function method()
    {
        return $_SERVER['REQUEST_METHOD'];
    }
}