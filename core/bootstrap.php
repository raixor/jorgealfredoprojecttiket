<?php
/**
 * Created by PhpStorm.
 * User: georgealmeida
 * Date: 9/11/17
 * Time: 19:01
 */

use ticket\core\App;
use ticket\core\database\Connection;
use ticket\core\database\QueryBuilder;
session_start();

$config = require __DIR__ . '/../app/config.php';
App:: bind ('config', $config);

App:: bind ('database', new QueryBuilder(
    Connection:: make ($config['database'])
));