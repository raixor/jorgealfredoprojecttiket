<?php
/**
 * Created by PhpStorm.
 * User: georgealmeida
 * Date: 9/11/17
 * Time: 19:14
 */

namespace ticket\core;

use Exception;

class Router
{
    private $routes = array(
        'GET' => array(),
        'POST' => array(),
        'DELETE' => array()
    );

    /**
     * @param $file
     */
    public static function load($file)
    {
        $router = new static;
        require $file;
        App:: bind ('router', $router);
    }

    public function get($uri, $controller, $rol='comprador')
    {
        $this->routes['GET'][$uri] =  array(
            'controller'=>$controller,
            'rol'=>$rol
        );
    }

    public function post($uri, $controller,  $rol='comprador')
    {
        $this->routes['POST'][$uri] = array(
            'controller'=>$controller,
            'rol'=>$rol
        );
    }

    public function delete($uri, $controller, $rol='comprador')
    {
        $this->routes['DELETE'][$uri] = array(
            'controller'=>$controller,
            'rol'=>$rol
        );
    }

    public function define($routes)
    {
        $this->routes = $routes;
    }


    private function callAction($controller, $action, $parameters=[])
    {
        $controller = "ticket\\app\\controllers\\" . $controller;
        $objController = new $controller;

        if (!method_exists($objController, $action)) {
            throw new Exception(
                "El controlador $controller no responde al action $action");
        }

        return call_user_func_array(array($objController, $action), $parameters);
    }

    private function getParametersRoute(string $route, array $matches)
    {
        preg_match_all('/:([^\/]+)/', $route, $parameterNames);
        return array_intersect_key($matches, array_flip($parameterNames[1]));
    }

    private function prepareRoute(string $route)
    {
        $urlRule = preg_replace (
            '/:([^\/]+)/',
            '(?<\1>[^/]+)',
            $route
        );

        return str_replace ('/', '\/', $urlRule);
    }


    public function direct($uri, $method)
    {
        foreach($this->routes[$method] as $route=>$routeData)
        {
            $urlRule = $this->prepareRoute($route);
            if (preg_match('/^' . $urlRule . '\/*$/s', $uri, $matches))
            {
                if (Security::isUserGranted($routeData['rol']) === false)
                {
                    if (!is_null(App::get('user')))
                        $this->callAction('AuthController','unauthorized');
                    else
                        $this->redirect('login');
                    return;
                }
                else
                {
                    $parameters = $this->getParametersRoute($route, $matches);
                    list($controller, $action) = explode ('@', $routeData['controller']);

                    return $this->callAction($controller, $action, $parameters);
                }
            }
        }
        $this->callAction('PagesControllers','notFound');
    }

    public function redirect($uri)
    {
        header('location: /' . $uri);
        exit();
    }
}