<?php
/**
 * Created by PhpStorm.
 * User: georgealmeida
 * Date: 9/11/17
 * Time: 19:34
 */

require '../vendor/autoload.php';

use ticket\core\App;
use ticket\core\Request;
use ticket\core\Router;
//idioma

require '../core/bootstrap.php';
echo $_SERVER['HTTP_ACCEPT_LANGUAGE'];
if(isset($_SESSION['language']) === true)
    $language = $_SESSION['language'];
else
    $language = "en_GB.utf8";

putenv("LC_ALL=$language");
setlocale(LC_ALL, $language);

bindtextdomain("en_GB", "../locale");
bind_textdomain_codeset("en_GB", "UTF-8");
textdomain("en_GB");

$log = new Monolog\Logger($config['logs']['name']);
$log->pushHandler(
    new Monolog\Handler\StreamHandler(
        $config['logs']['file'],
        Monolog\Logger:: WARNING )
);

if (isset($_SESSION['usuario']))
{
    $usuario = App::get('database')->find('Usuarios', 'Usuario', $_SESSION['usuario']);
}
else
    $usuario = null;
App::bind('user', $usuario);

try
{
    Router::load(__DIR__.'/../app/routes.php');
    App::get('router')->direct(Request::uri(), Request::method());
}
catch(Exception $ex)
{
    $log->addError($ex->getMessage());
}