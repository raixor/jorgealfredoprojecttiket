window.addEventListener('load', () => {
    let btnLogin = document.getElementById('btn-login');
    let btnRegister = document.getElementById('btn-register');
    let btnFiltros = document.getElementById('filters');

    let filterModal = document.getElementById('filterModal');
    let btnCloseFilter = document.getElementById('closeFilter');

    let loginModal = document.getElementById('loginModal');
    let registerModal = document.getElementById('registroModal');
    let btnRegistro = document.getElementById('btn-registro');

    let btnCloseLogin = document.getElementById('closeLogin');
    let btnCloseRegister = document.getElementById('closeRegister');

    let btnOpenLogin = document.getElementById('registroLogear');

    let imgEventDetail = document.querySelector('.card.edit.detail');
    if(imgEventDetail){
        /*imgEventDetail.style.backgroundImage = "url('')";*/
    }
    if(btnOpenLogin){
        divRegistrarse = document.querySelector('#formLogin .registrarse');

        btnOpenLogin.addEventListener('click', (event)=>{
            loginModal.style.display = 'flex';
            divRegistrarse.style.display = 'none';
        });
        btnCloseLogin.addEventListener('click', (event) => {
            loginModal.style.display = 'none';
        });
        window.addEventListener('click', (event) => {
            if (event.target == loginModal) {
                loginModal.style.display = 'none';
            }
        });
    }
    if(btnFiltros){

        btnFiltros.addEventListener('click', (event)=>{
            if (loginModal.style.display === 'flex') {
                loginModal.style.display = 'none';
                return;
            }
            if(filterModal.style.display === 'flex'){
                filterModal.style.display = 'none';
                return
            }
            if (registerModal.style.display === 'flex') {
                registerModal.style.display = 'none';
                return;
            }
            filterModal.style.display = 'flex';

        });

        btnCloseFilter.addEventListener('click', (event) => {
            filterModal.style.display = 'none';
        });
        window.addEventListener('click', (event) => {
            if (event.target == filterModal) {
                filterModal.style.display = 'none';
            }
        });
    }

    if (btnLogin) {


        btnCloseLogin.addEventListener('click', (event) => {
            loginModal.style.display = 'none';
        });
        btnCloseRegister.addEventListener('click', (event) => {
            registerModal.style.display = 'none';
        });

        window.addEventListener('click', (event) => {
            if (event.target == loginModal || event.target == registerModal) {
                registerModal.style.display = 'none';
                loginModal.style.display = 'none';
            }
        });

       btnRegister.addEventListener('click', (event)=>{
           event.preventDefault();

           if (loginModal.style.display === 'flex') {
               loginModal.style.display = 'none';
               return;
           }
           if(filterModal.style.display === 'flex'){
               filterModal.style.display = 'none';
               return
           }
           if (registerModal.style.display === 'flex') {
               registerModal.style.display = 'none';
               return;
           }

           registerModal.style.display = 'flex';

       });
        btnLogin.addEventListener('click', (event) => {
            event.preventDefault();

            if (loginModal.style.display === 'flex') {
                loginModal.style.display = 'none';
                return;
            }
            if(filterModal.style.display === 'flex'){
                filterModal.style.display = 'none';
                return
            }
            if (registerModal.style.display === 'flex') {
                registerModal.style.display = 'none';
                return;
            }

            loginModal.style.display = 'flex';

            btnRegistro.addEventListener('click', (event) => {
                event.preventDefault();
                registerModal.style.display = 'flex';
                loginModal.style.display = 'none';
            });
        });
    }

    /*card en tiempo real*/

    if (document.getElementById('newEvent')) {
        let nameEvent = document.getElementById('newEvent').nombre;
        let priceEvent = document.getElementById('newEvent').precio;
        let descriptionEvent = document.getElementById('newEvent').descripcion;
        let imgEvent = document.getElementById('newEvent').imagen;

        let cardEdit = document.getElementById('card-edit');
        if (nameEvent) {
            console.log("entro");

            let title = document.querySelector('.card.edit .card-title.text-dark');
            let price = document.querySelector('.card.edit .price p:not(.euro)');
            let description = document.querySelector('.card.edit .card-content p');

            imgEvent.addEventListener('change', (event) => {
                let file = event.target.files[0];
                let reader = new FileReader();

                if (file) reader.readAsDataURL(file);

                reader.addEventListener('load', e => {
                    document.getElementById('img-card').src = reader.result;
                }, false);
            });
            console.log(description);
            nameEvent.addEventListener('input', (event) => {
                title.innerText = event.target.value;
            });
            priceEvent.addEventListener('input', (event) => {
                if (event.target.value.length < 4)
                    price.innerText = event.target.value;
            });
            descriptionEvent.addEventListener('input', (event) => {
                console.log("a");
                description.innerText = event.target.value;
            });
        }
        console.log(cardEdit);
    }


    if (document.getElementById('ico-login no-login')) {
        document.getElementById('ico-login no-login').addEventListener('click', (event) => {
            event.preventDefault();
            if (loginModal.style.display === 'flex') {
                loginModal.style.display = 'none';
                return;
            }
            let registerModal = document.getElementById('registroModal');

            console.log(registerModal);
            if (registerModal.style.display === 'flex') {
                registerModal.style.display = 'none';
                return;
            }

            document.getElementById('loginModal').style.display = 'flex';

            document.getElementById('btn-registro').addEventListener('click', (event) => {
                event.preventDefault();
                document.getElementById('registroModal').style.display = 'flex';
                document.getElementById('loginModal').style.display = 'none';
            });
        });
    }

    let search = document.getElementById('search');
    let menuMObile = document.querySelector('.menu-panel.mobile');
    let topbar = document.querySelector('.back-panel');
    // search.addEventListener('focus', (event) => {
    //    console.log(menuMObile);
    //    menuMObile.style.display = 'none';
    // })

    // search.addEventListener('focusout', (event) =>{
    //    menuMObile.style.display = 'flex';
    //    console.log(menuMObile);

    // })

    let start = 0;
    document.addEventListener('touchstart', (e) => {
        let touchobj = e.changedTouches[0];
        start = touchobj.clientY;

    })

    document.addEventListener('touchmove', (e) => {
        if (start < e.changedTouches[0].clientY) {
            console.log("start:");
            menuMObile.style.display = 'none';
        } else {
            menuMObile.style.display = 'flex';
        }

        if (start > e.changedTouches[0].clientY) {
            console.log("start:");
            topbar.style.display = 'none';
        } else {
            topbar.style.display = 'flex';
        }
    })
    // document.addEventListener('scroll', (event)=>{
    //    let scrollNumber = document.scrollingElement.scrollTop;
    //    console.log(scrollNumber);
    //    if(scrollNumber > 0){
    //       menuMObile.style.display = 'none';
    //    }
    // })
});

